protocol ChangeCommissionedTransaction: ChangeClassificationTransaction {
  var salary: Double { get }
  var commissionRate: Double { get }
}

extension ChangeCommissionedTransaction {
  func paymentClassification() -> PaymentClassification {
    return CommissionedClassification(salary: salary, commissionRate: commissionRate)
  }
  func paymentSchedule() -> PaymentSchedule {
    return BiweeklySchedule()
  }
}

struct ChangeCommissionedTransactionImpl: ChangeCommissionedTransaction {
  let employeeID: Int
  let salary: Double
  let commissionRate: Double
}
