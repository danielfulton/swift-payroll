import XCTest
@testable import Payroll

class TimeCardTransactionTests: XCTestCase {
  func testTimeCardTransaction() {
    let employeeID = 345
    let addTransaction = AddHourlyEmployeeTransaction(employeeID: employeeID, name: "Tom", address: "Home", hourlyRate: 15.25)
    addTransaction.execute()
    
    let date = Date.distantPast
    let hours: Double = 8.0
    let timecardTransaction = TimecardTransaction(date: date, hours: hours, employeeID: employeeID)
    timecardTransaction.execute()
    
    let employee = PayrollDatabase.getEmployee(employeeID)
    XCTAssertNotNil(employee)
    let pc = employee?.classification
    XCTAssertTrue(pc is HourlyClassification)
    let hourly = pc as? HourlyClassification
    let timecard = hourly?.getTimeCard(date)
    XCTAssertNotNil(timecard)
    XCTAssertEqual(hours, timecard?.hours())
  }
}
