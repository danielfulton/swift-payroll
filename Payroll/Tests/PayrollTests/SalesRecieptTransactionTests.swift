import XCTest
@testable import Payroll

class SalesReceiptTransactionTests: XCTestCase {
  func testAddSalesRecceiptTrasaction() {
    let employeeID = 456
    let addTransaction = AddCommissionedEmployeeTransaction(employeeID: employeeID, name: "Some Guy", address: "Home", salary: 1234, commissionRate: 0.1)
    addTransaction.execute()
    
    let date = Date.distantPast
    let amount: Double = 500
    let salesReceiptTransaction = SalesReceiptTransaction(date: date, amount: amount, employeeID: employeeID)
    salesReceiptTransaction.execute()
    
    let employee = PayrollDatabase.getEmployee(employeeID)
    XCTAssertNotNil(employee)
    
    let pc = employee?.classification
    XCTAssertTrue(pc is CommissionedClassification)
    let commissioned = pc as? CommissionedClassification
    let salesReceipt = commissioned?.getSalesReceipt(date)
    XCTAssertNotNil(salesReceipt)
    XCTAssertEqual(salesReceipt?.date, date)
  }
}
