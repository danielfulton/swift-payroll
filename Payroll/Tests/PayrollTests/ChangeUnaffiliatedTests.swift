import XCTest
@testable import Payroll

class ChangeUnaffiliatedTests: XCTestCase {
  let employeeID = 18273687765
  let dues: Double = 123123

  func testUnaffiliated() throws {
    let addEmployee = AddSalariedEmployee(employeeID: employeeID,
                                          name: "Dan",
                                          address: "Home",
                                          salary: 123)
    addEmployee.execute()
    let memberID = 4322
    let changeMember = ChangeMemberTransactionImpl(employeeID: employeeID,
                                                   memberID: memberID,
                                                   dues: dues)
    changeMember.execute()
    let unaffiliated = ChangeUnaffiliatedTransactionImpl(employeeID: employeeID, memberID: memberID)
    unaffiliated.execute()
    let employee = try XCTUnwrap(PayrollDatabase.getEmployee(employeeID))
    let affiliation = employee.affiliation
    XCTAssert(affiliation is NoAffiliation)
    XCTAssertNil(PayrollDatabase.getUnionMember(memberID))
  }
}
