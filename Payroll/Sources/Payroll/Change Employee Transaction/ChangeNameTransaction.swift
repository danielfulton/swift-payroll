protocol ChangeNameTransaction: ChangeEmployeeTransaction {
  var newName: String { get }
}

extension ChangeNameTransaction {
  func changeEmployee(_ employee: Employee) {
    var changedEmployee = employee
    changedEmployee.name = newName
    PayrollDatabase.deleteEmployee(employee.id)
    PayrollDatabase.addEmployee(changedEmployee)
  }
}

struct ChangeNameTransactionImpl: ChangeNameTransaction {
  let employeeID: Int
  let newName: String
}
