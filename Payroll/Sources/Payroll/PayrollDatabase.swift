public struct PayrollDatabase {
  public typealias EmployeeID = Int
  fileprivate static var employees: [EmployeeID: Employee] = [:]
  fileprivate static var unionMembers: [Int:Employee] = [:]
  public static func getEmployee(_ id: Int) -> Employee? {
    return PayrollDatabase.employees[id]
  }
  public static func addEmployee(_ employee: Employee) {
    PayrollDatabase.employees[employee.id] = employee
  }
  public static func addUnionMember(_ memberID: Int, _ employee: Employee) {
    unionMembers[memberID] = employee
  }
  public static func removeUnionMember(_ memberID: Int) {
    unionMembers.removeValue(forKey: memberID)
  }
  public static func getUnionMember(_ memberID: Int) -> Employee? {
    return unionMembers[memberID]
  }
  public static func reset() {
    PayrollDatabase.employees = [:]
  }
  public static func deleteEmployee(_ employeeID: EmployeeID) {
    PayrollDatabase.employees.removeValue(forKey: employeeID)
  }
  public static func getAllEmployeeIds() -> [EmployeeID] {
    return employees.keys.map({ $0 as EmployeeID })
  }
}
