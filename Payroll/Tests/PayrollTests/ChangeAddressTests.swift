import XCTest
@testable import Payroll

class ChangeAddressTransactionTests: XCTestCase {
  func testChangeAddress() {
    let employeeID = 8
    let addEmployee = AddHourlyEmployeeTransaction(employeeID: employeeID,
                                                   name: "Dave",
                                                   address: "Home",
                                                   hourlyRate: 15.25)
    addEmployee.execute()
    
    let changeAddress = ChangeAddressTransactionImpl(employeeID: employeeID,
                                                     newAddress: "Work")
    changeAddress.execute()
    
    let employee = PayrollDatabase.getEmployee(employeeID)
    
    XCTAssertNotNil(employee)
    XCTAssertEqual(employee?.address, "Work")
  }
}
