import XCTest
@testable import Payroll
class PayComissionedEmployeeTests: XCTestCase {
  func testPayCommissionedEmployeeNoSalesReceipts() throws {
    let empID = 123
    let commy = AddCommissionedEmployeeTransaction(employeeID: empID, name: "Dan", address: "Home", salary: 2000, commissionRate: 0.1)
    commy.execute()
    let friday = try date(year: 2021, month: 12, day: 31)
    let payday = PaydayTransaction(payDate: friday)
    payday.execute()
    let paycheck = try XCTUnwrap(payday.getPaycheck(empID))
    XCTAssertEqual(paycheck.grossPay, 2000)
    XCTAssertEqual(paycheck.netPay, 2000)
    XCTAssertEqual(0.0, paycheck.deductions, accuracy: 0.001)
  }
  
  func testCommissionedEmployeeOneSalesReceipt() throws {
    let empID = 123
    let commy = AddCommissionedEmployeeTransaction(employeeID: empID, name: "Dan", address: "Home", salary: 2000, commissionRate: 0.1)
    commy.execute()
    
    let friday = try date(year: 2021, month: 12, day: 31)
    
    let receipt = SalesReceiptTransaction(date: friday, amount: 100, employeeID: empID)
    receipt.execute()
    
    let payday = PaydayTransaction(payDate: friday)
    payday.execute()
    let paycheck = try XCTUnwrap(payday.getPaycheck(empID))
    XCTAssertEqual(paycheck.grossPay, 2010, accuracy: 0.001)
    XCTAssertEqual(paycheck.netPay, 2010, accuracy: 0.001)
    XCTAssertEqual(0.0, paycheck.deductions, accuracy: 0.001)
  }
  
  func testCommissionedEmployeeSalesReceiptOutOfPayPeriod() throws {
    let empID = 123
    let commy = AddCommissionedEmployeeTransaction(employeeID: empID, name: "Dan", address: "Home", salary: 2000, commissionRate: 0.1)
    commy.execute()
    
    let friday = try date(year: 2021, month: 12, day: 31)
    let lastMonth = try date(year: 2021, month: 11, day: 31)
    
    let receipt = SalesReceiptTransaction(date: lastMonth, amount: 100, employeeID: empID)
    receipt.execute()
    
    let payday = PaydayTransaction(payDate: friday)
    payday.execute()
    let paycheck = try XCTUnwrap(payday.getPaycheck(empID))
    XCTAssertEqual(paycheck.grossPay, 2000, accuracy: 0.001)
    XCTAssertEqual(paycheck.netPay, 2000, accuracy: 0.001)
    XCTAssertEqual(0.0, paycheck.deductions, accuracy: 0.001)
  }
}
