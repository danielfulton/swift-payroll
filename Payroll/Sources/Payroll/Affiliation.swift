import Foundation

protocol Affiliation {
  func getServiceCharge(_ date: Date) -> ServiceCharge?
  func calculateDeductions(_ paycheck: Paycheck) -> Double
}
