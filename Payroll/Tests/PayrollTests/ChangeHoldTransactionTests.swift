import XCTest
@testable import Payroll

class ChangeHoldTransactionTests: XCTestCase {
  let employeeID = 1827362367
  
  func testChangeHold() {
    addEmployee()
    changeToDirect()
    changeToHold()
    let employee = PayrollDatabase.getEmployee(employeeID)
    XCTAssertTrue(employee?.method is HoldMethod)
  }
  
  fileprivate func addEmployee() {
    let addEmployee = AddHourlyEmployeeTransaction(employeeID: employeeID,
                                                   name: "Dan",
                                                   address: "Home",
                                                   hourlyRate: 12)
    addEmployee.execute()
  }
  
  fileprivate func changeToDirect() {
    let changeDirect = ChangeDirectTransactionImpl(employeeID: employeeID,
                                                   bank: "",
                                                   account: "")
    changeDirect.execute()
  }
  
  fileprivate func changeToHold() {
    let changeHold = ChangeHoldTransactionImpl(employeeID: employeeID)
    changeHold.execute()
  }
}
