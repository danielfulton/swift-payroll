import Foundation

class UnionAffiliation: Affiliation {
  fileprivate var charges: [ServiceCharge] = []
  public let dues: Double
  let memberID: Int
  init(memberID: Int, dues: Double) {
    self.dues = dues
    self.memberID = memberID
  }
  func getServiceCharge(_ date: Date) -> ServiceCharge? {
    return charges.first(where: { $0.date == date })
  }
  func addServiceCharge(_ date: Date, amount: Double) {
    charges.append(.init(date: date, amount: amount))
  }
  
  func calculateDeductions(_ paycheck: Paycheck) -> Double {
    let fridays = DateUtil.numberOfFridaysInPayPeriod(paycheck.startDate, paycheck.payDate)
    return (Double(fridays) * dues) + serviceCharges(paycheck)
  }
  
  fileprivate func serviceCharges(_ paycheck: Paycheck) -> Double {
    return chargesInPayPeriod(paycheck).map({ $0.amount }).reduce(0, +)
  }
  
  fileprivate func chargesInPayPeriod(_ paycheck: Paycheck) -> [ServiceCharge] {
    return charges.filter({ DateUtil.isInPayPeriod(date: $0.date, startDate: paycheck.startDate, endDate: paycheck.payDate) })
  }
  

}
