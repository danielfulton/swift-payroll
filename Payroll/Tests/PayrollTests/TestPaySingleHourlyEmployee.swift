import XCTest
@testable import Payroll

class TestPaySingleHourlyEmployee: XCTestCase {
  let employeeID = 4234

  func testPaySingleHourlyEmployeeNoTimecards() throws {
    let addEmployee = AddHourlyEmployeeTransaction(employeeID: employeeID,
                                                   name: "Dan",
                                                   address: "Home",
                                                   hourlyRate: 15.25)
    addEmployee.execute()
    let paydate = try date(year: 2021, month: 12, day: 24)
    let payDayTransaction = PaydayTransaction(payDate: paydate)
    payDayTransaction.execute()
    try validateHourlyPaycheck(payDayTransaction,
                               employeeID,
                               paydate,
                               pay: 0)
  }
  
  func testPaySingleHourlyEmployeeOneTimecard() throws {
    let add = AddHourlyEmployeeTransaction(employeeID: employeeID,
                                           name: "Dan",
                                           address: "Home",
                                           hourlyRate: 15.25)
    add.execute()
    let friday = try date(year: 2021, month: 12, day: 24)
    let timeCard = TimecardTransaction(date: friday,
                                       hours: 2.0,
                                       employeeID: employeeID)
    timeCard.execute()
    let payday = PaydayTransaction(payDate: friday)
    payday.execute()
    try validateHourlyPaycheck(payday,
                               employeeID,
                               friday,
                               pay: 30.5)
  }
  
  func testPaySingleHourlyEmployeeOvertimeOneTimeCard() throws {
    let add = AddHourlyEmployeeTransaction(employeeID: employeeID, name: "Dan", address: "Home", hourlyRate: 15.25)
    add.execute()
    let paydate = try date(year: 2021, month: 12, day: 24)
    let timeCard = TimecardTransaction(date: paydate, hours: 9.0, employeeID: employeeID)
    timeCard.execute()
    let payday = PaydayTransaction(payDate: paydate)
    payday.execute()
    try validateHourlyPaycheck(payday, employeeID, paydate, pay: (8 + 1.5) * 15.25)
  }
  
  func testPaySingleHourlyEmployeeOnWrongDate() throws {
    let add = AddHourlyEmployeeTransaction(employeeID: employeeID, name: "Dan", address: "Home", hourlyRate: 15.25)
    add.execute()
    let thursday = try date(year: 2021, month: 12, day: 23)
    let timeCard = TimecardTransaction(date: thursday, hours: 9, employeeID: employeeID)
    timeCard.execute()
    let payday = PaydayTransaction(payDate: thursday)
    payday.execute()
    XCTAssertNil(payday.getPaycheck(employeeID))
  }
  
  func testPaySingleHourlyEmployeeTwoTimeCards() throws {
    let add = AddHourlyEmployeeTransaction(employeeID: employeeID, name: "Dan", address: "Home", hourlyRate: 15.25)
    add.execute()
    let friday = try date(year: 2021, month: 12, day: 24)
    let thursday = try date(year: 2021, month: 12, day: 23)
    let timeCard = TimecardTransaction(date: friday, hours: 2, employeeID: employeeID)
    timeCard.execute()
    let timecard2 = TimecardTransaction(date: thursday, hours: 5, employeeID: employeeID)
    timecard2.execute()
    
    let payday = PaydayTransaction(payDate: friday)
    payday.execute()
    try validateHourlyPaycheck(payday, employeeID, friday, pay: 7 * 15.25)
  }
  
  func testPaySingleHourlyEmployeeWithTimeCardsSpanningTwoTimePeriods() throws {
    let add = AddHourlyEmployeeTransaction(employeeID: employeeID, name: "Dan", address: "Home", hourlyRate: 15.25)
    add.execute()
    let friday = try date(year: 2021, month: 12, day: 24)
    let lastWeek = try date(year: 2021, month: 12, day: 17)
    let timeCard = TimecardTransaction(date: friday, hours: 2, employeeID: employeeID)
    timeCard.execute()
    let previousTimeCard = TimecardTransaction(date: lastWeek, hours: 5, employeeID: employeeID)
    previousTimeCard.execute()
    let payday = PaydayTransaction(payDate: friday)
    payday.execute()
    try validateHourlyPaycheck(payday, employeeID, friday, pay: 2 * 15.25)
  }
  
  fileprivate func validateHourlyPaycheck(_ payDayTransaction: PaydayTransaction, _ employeeID: Int, _ paydate: Date, pay: Double) throws {
    let paycheck = try XCTUnwrap(payDayTransaction.getPaycheck(employeeID))
    XCTAssertNotNil(paycheck)
    XCTAssertEqual(paydate, paycheck.payDate)
    XCTAssertEqual(paycheck.grossPay, pay, accuracy: 0.001)
    XCTAssertEqual("Hold", paycheck.getField("Disposition"))
    XCTAssertEqual(0, paycheck.deductions, accuracy:0.001)
    XCTAssertEqual(paycheck.netPay, pay, accuracy: 0.001)
  }
}

extension XCTestCase {
  func date(year: Int, month: Int, day: Int) throws -> Date {
    let calendar = Calendar(identifier: .gregorian)
    let timeZone = TimeZone(secondsFromGMT: 0)
    let components: DateComponents = DateComponents(calendar: calendar,
                                                    timeZone: timeZone,
                                                    era: nil,
                                                    year: year,
                                                    month: month,
                                                    day: day)
    return try XCTUnwrap(components.date)
  }
}
