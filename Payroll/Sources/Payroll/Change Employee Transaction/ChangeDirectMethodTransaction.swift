protocol ChangeDirectTransaction: ChangeMethodTransaction {
  var bank: String { get }
  var account: String { get }
}

struct ChangeDirectTransactionImpl: ChangeDirectTransaction {
  let employeeID: Int
  let bank: String
  let account: String
  
  func getMethod() -> PaymentMethod {
    return DirectMethod(bank: bank, account: account)
  }
}
