struct AddHourlyEmployeeTransaction: AddEmployeeTransaction {
  let employeeID: Int
  let name: String
  let address: String
  let hourlyRate: Double
  func makeClassification() -> PaymentClassification {
    return HourlyClassification(hourlyRate: hourlyRate)
  }
  
  func makeSchedule() -> PaymentSchedule {
    return WeeklySchedule()
  }
  
  
}
