import Foundation

struct NoAffiliation: Affiliation, Equatable {
  func getServiceCharge(_ date: Date) -> ServiceCharge? {
    return nil
  }
  
  func calculateDeductions(_ paycheck: Paycheck) -> Double {
    return 0
  }
}
