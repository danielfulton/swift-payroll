protocol ChangeClassificationTransaction: ChangeEmployeeTransaction {
  func paymentClassification() -> PaymentClassification
  func paymentSchedule() -> PaymentSchedule
}

extension ChangeClassificationTransaction {
  func changeEmployee(_ employee: Employee) {
    var changedEmployee = employee
    changedEmployee.classification = paymentClassification()
    changedEmployee.schedule = paymentSchedule()
    PayrollDatabase.deleteEmployee(employeeID)
    PayrollDatabase.addEmployee(changedEmployee)
  }
}
