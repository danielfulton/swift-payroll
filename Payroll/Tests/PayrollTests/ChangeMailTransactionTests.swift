import XCTest
@testable import Payroll

class ChangeMailTransactionTests: XCTestCase {
  func testChangeMail() {
    let employeeID = 18273623
    let addEmployee = AddHourlyEmployeeTransaction(employeeID: employeeID,
                                                   name: "Dan",
                                                   address: "Home",
                                                   hourlyRate: 12)
    addEmployee.execute()
    let newAddress = "Work"
    let changeMail: ChangeMailTransaction = ChangeMailTransactionImpl(employeeID: employeeID, address: newAddress)
    changeMail.execute()
    let employee = PayrollDatabase.getEmployee(employeeID)
    XCTAssertNotNil(employee)
    let paymentMethod = employee?.method
    XCTAssertTrue(paymentMethod is MailMethod)
    let mailMethod = paymentMethod as? MailMethod
    XCTAssertEqual(mailMethod?.address, newAddress)
  }
}
