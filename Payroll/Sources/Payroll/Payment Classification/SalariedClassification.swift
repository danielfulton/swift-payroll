struct SalariedClassification: PaymentClassification, Equatable {
  let salary: Double
  func calculatePay(_ paycheck: Paycheck) -> Double {
    return salary
  }
}
