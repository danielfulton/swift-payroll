protocol PaymentMethod {
  func pay(_ paycheck: Paycheck)
}
