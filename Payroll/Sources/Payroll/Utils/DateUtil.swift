//
//  File.swift
//  
//
//  Created by FultonDaniel on 2022/01/13.
//

import Foundation

struct DateUtil {
  static func isInPayPeriod(date: Date, startDate: Date, endDate: Date) -> Bool {
    return (date >= startDate) && (date <= endDate)
  }
  
  static func numberOfFridaysInPayPeriod(_ startDate: Date, _ endDate: Date) -> Int {
    let calendar = Calendar(identifier: .gregorian)
    let loopDayComps = DateComponents(calendar: calendar, weekday: 6)
    var fridays = 0
    calendar.enumerateDates(startingAfter: startDate, matching: loopDayComps, matchingPolicy: .strict) { (date, _, stop) in
      guard let currentDate = date else {
        stop = true
        return
      }
      if currentDate <= endDate {
        fridays += 1
      }
      if currentDate >= endDate {
        stop = true
      }
    }
    print(fridays)
    return fridays
  }
}
