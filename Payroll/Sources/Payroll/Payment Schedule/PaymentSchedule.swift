import Foundation

protocol PaymentSchedule {
  func isPayDate(_ date: Date) -> Bool
  func getPayPeriodStartDate(_ date: Date) -> Date
}
