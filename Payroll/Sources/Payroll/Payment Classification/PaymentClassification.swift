import Foundation

protocol PaymentClassification {
  func calculatePay(_ paycheck: Paycheck) -> Double
}

//extension PaymentClassification {
////  func isInPayPeriod(_ date: Date, _ payCheck: Paycheck) -> Bool {
////    let startDate = payCheck.startDate
////    let endDate = payCheck.payDate
////    return (date >= startDate) && (date <= endDate)
////  }
//}
