struct AddCommissionedEmployeeTransaction: AddEmployeeTransaction {
  let employeeID: Int
  let name: String
  let address: String
  let salary: Double
  let commissionRate: Double
  func makeClassification() -> PaymentClassification {
    return CommissionedClassification(salary: salary, commissionRate: commissionRate)
  }
  
  func makeSchedule() -> PaymentSchedule {
    return BiweeklySchedule()
  }
}
