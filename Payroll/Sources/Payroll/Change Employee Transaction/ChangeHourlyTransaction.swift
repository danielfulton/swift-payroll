protocol ChangeHourlyTransaction: ChangeClassificationTransaction {
  var hourlyRate: Double { get }
}

struct ChangeHourlyTransactionImpl: ChangeHourlyTransaction {
  var employeeID: Int
  var hourlyRate: Double

  func paymentClassification() -> PaymentClassification {
    return HourlyClassification(hourlyRate: hourlyRate)
  }
  
  func paymentSchedule() -> PaymentSchedule {
    return WeeklySchedule()
  }
}
