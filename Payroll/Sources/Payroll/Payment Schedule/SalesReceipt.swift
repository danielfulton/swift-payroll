import Foundation

struct SalesReceipt: Equatable {
  let date: Date
  let amount: Double
}
