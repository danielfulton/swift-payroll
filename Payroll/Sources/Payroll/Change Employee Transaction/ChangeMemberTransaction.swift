protocol ChangeMemberTransaction: ChangeAffiliationTransaction {
  var memberID: Int { get }
  var dues: Double { get }
}

extension ChangeMemberTransaction {
  func getAffiliation() -> Affiliation {
    return UnionAffiliation(memberID: memberID, dues: dues)
  }
  
  func recordMembership(employee: Employee) {
    PayrollDatabase.addUnionMember(memberID, employee)
  }
}

struct ChangeMemberTransactionImpl: ChangeMemberTransaction {
  let employeeID: Int
  let memberID: Int
  let dues: Double
}
