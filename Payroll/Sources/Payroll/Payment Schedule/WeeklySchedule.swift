import Foundation

struct WeeklySchedule: PaymentSchedule {
  func isPayDate(_ date: Date) -> Bool {
    let cal = Calendar(identifier: .gregorian)
    let dayOfWeek = cal.component(.weekday, from: date)
    return dayOfWeek == 6
  }
  
  func getPayPeriodStartDate(_ date: Date) -> Date {
    return Calendar(identifier: .gregorian).date(byAdding: .day, value: -4, to: date)!
  }
}
