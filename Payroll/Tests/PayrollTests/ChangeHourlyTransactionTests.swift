import XCTest
@testable import Payroll

class ChangeHourlyTransactionTest: XCTestCase {
  func testChangeHourly() {
    let employeeID = 9
    let addCommissioned = AddCommissionedEmployeeTransaction(employeeID: employeeID, name: "Dan", address: "Home", salary: 122, commissionRate: 0.1)
    addCommissioned.execute()
    
    let changeHourly:ChangeHourlyTransaction = ChangeHourlyTransactionImpl(employeeID: employeeID, hourlyRate: 20.15)
    changeHourly.execute()
    let employee = PayrollDatabase.getEmployee(employeeID)
    
    XCTAssertNotNil(employee)
    let classification = employee?.classification
    XCTAssertNotNil(classification)
    XCTAssertTrue(classification is HourlyClassification)
    let hourly = classification as? HourlyClassification
    XCTAssertEqual(hourly?.hourlyRate(), 20.15)
    let paymentSchedule = employee?.schedule
    XCTAssertTrue(paymentSchedule is WeeklySchedule)
  }
}
