struct DeleteEmployeeTransaction: Command {
  let employeeID: Int
  func execute() {
    PayrollDatabase.deleteEmployee(employeeID)
  }
}
