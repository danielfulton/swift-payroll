import Foundation

struct MonthlySchedule: PaymentSchedule {
  let cal = Calendar(identifier: .gregorian)

  func isPayDate(_ date: Date) -> Bool {
    return isLastDayOfMonth(date)
  }

  func getPayPeriodStartDate(_ date: Date) -> Date {
    var comps = Calendar(identifier: .gregorian).dateComponents([.year,.month,.day], from: date)
    comps.day = 1
    return Calendar(identifier: .gregorian).date(from: comps)!
  }
  
  func isLastDayOfMonth(_ date: Date) -> Bool {
    return month(date) != month(nextDay(date)!)
  }
  
  fileprivate func month(_ date: Date) -> Int {
    return cal.component(.month, from: date)
  }
  
  fileprivate func nextDay(_ date: Date) -> Date? {
    return cal.date(byAdding: .day, value: 1, to: date)
  }
}
