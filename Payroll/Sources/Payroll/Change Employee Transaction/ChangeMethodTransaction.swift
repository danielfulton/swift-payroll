protocol ChangeMethodTransaction: ChangeEmployeeTransaction {
  func getMethod() -> PaymentMethod
}

extension ChangeMethodTransaction {
  func changeEmployee(_ employee: Employee) {
    let method = getMethod()
    var changedEmployee = employee
    changedEmployee.method = method
    PayrollDatabase.deleteEmployee(employeeID)
    PayrollDatabase.addEmployee(changedEmployee)
  }
}
