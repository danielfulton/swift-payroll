import XCTest
@testable import Payroll

class ChangeCommissionedTransactionTests: XCTestCase {
  func testChangeCommissioned() {
    let employeeID = 1231233
    let newSalary:Double = 111
    let addHourly = AddHourlyEmployeeTransaction(employeeID: employeeID, name: "Dan", address: "Home", hourlyRate: 143)
    addHourly.execute()
    let changeCommissioned: ChangeCommissionedTransaction = ChangeCommissionedTransactionImpl(employeeID: employeeID, salary: newSalary, commissionRate: 0.1)
    changeCommissioned.execute()
    let employee = PayrollDatabase.getEmployee(employeeID)
    XCTAssertNotNil(employee)
    let pc = employee?.classification
    XCTAssertTrue(pc is CommissionedClassification)
    let commissioned = pc as? CommissionedClassification
    XCTAssertEqual(commissioned?.salary, newSalary)
  }
}
