import Foundation

struct TimeCard: Equatable {
  private let _date: Date
  private let _hours: Double
  init(date: Date, hours: Double) {
    _date = date
    _hours = hours
  }
  func hours() -> Double {
    return _hours
  }
  func date() -> Date {
    return _date
  }
}
