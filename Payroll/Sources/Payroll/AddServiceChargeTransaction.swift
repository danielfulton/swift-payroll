import Foundation

struct ServiceChargeTransaction: Command {
  let memberID: Int
  let date: Date
  let amount: Double
  func execute() {
    guard var employee = PayrollDatabase.getUnionMember(memberID) else {
      print("Error: No such union member.")
      return
    }
    guard let unionAffiliation = employee.affiliation as? UnionAffiliation else {
      print("Error: Tried to add service charge to non-union member.")
      return
    }
    unionAffiliation.addServiceCharge(date, amount: amount)
    employee.affiliation = unionAffiliation
    PayrollDatabase.addUnionMember(memberID, employee)
  }
}
