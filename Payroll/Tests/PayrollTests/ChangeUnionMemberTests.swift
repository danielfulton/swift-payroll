import XCTest
@testable import Payroll

class ChangeUnionMemberTests: XCTestCase {
  let employeeID = 18273687765
  let dues: Double = 123123
  func testChangeUnionMember() throws {
    let addEmployee = AddSalariedEmployee(employeeID: employeeID,
                                          name: "Dan",
                                          address: "Home",
                                          salary: 123)
    addEmployee.execute()
    let memberID = 4322
    let changeMember = ChangeMemberTransactionImpl(employeeID: employeeID,
                                                   memberID: memberID,
                                                   dues: dues)
    changeMember.execute()
    
    let employee = try XCTUnwrap(PayrollDatabase.getEmployee(employeeID))
    let affiliation = employee.affiliation
    XCTAssertNotNil(affiliation)
    XCTAssertTrue(affiliation is UnionAffiliation)
    let union = try XCTUnwrap(affiliation as? UnionAffiliation)
    XCTAssertEqual(dues, union.dues, accuracy: 0.001)
    let member = try XCTUnwrap(PayrollDatabase.getUnionMember(memberID))
    XCTAssertEqual(member.id, employee.id)
    XCTAssertNotNil(member.affiliation)
    XCTAssertTrue(member.affiliation is UnionAffiliation)
  }
}
