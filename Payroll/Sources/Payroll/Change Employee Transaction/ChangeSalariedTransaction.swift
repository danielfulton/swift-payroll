protocol ChangeSalariedTransaction: ChangeClassificationTransaction {
  var salary: Double { get }
}

extension ChangeSalariedTransaction {
  func paymentClassification() -> PaymentClassification {
    return SalariedClassification(salary: salary)
  }
  
  func paymentSchedule() -> PaymentSchedule {
    MonthlySchedule()
  }
}

struct ChangeSalariedTransactionImpl: ChangeSalariedTransaction {
  let employeeID: Int
  let salary: Double
}
