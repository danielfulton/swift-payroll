import XCTest
@testable import Payroll
class AddEmployeeTests: XCTestCase {
  override func setUp() {
    super.setUp()
    PayrollDatabase.reset()
  }
  func testAddSalariedEmployee() {
    let employeeID = 123
    let transaction = AddSalariedEmployee(employeeID: employeeID,
                                          name: "Dan",
                                          address: "Home",
                                          salary: 1000.0)
    transaction.execute()
    let employee = PayrollDatabase.getEmployee(employeeID)
    XCTAssertEqual("Dan", employee?.name)
    let paymentClassification = employee?.classification
    XCTAssertTrue(paymentClassification is SalariedClassification)
    let salaried = paymentClassification as? SalariedClassification
    XCTAssertEqual(salaried?.salary, 1000.0)
    XCTAssertTrue(employee?.schedule is MonthlySchedule)
    XCTAssertTrue(employee?.method is HoldMethod)
  }
  
  func testAddHourlyEmployee() {
    let employeeID = 123
    let transaction = AddHourlyEmployeeTransaction(employeeID: employeeID,
                                                   name: "Daniel",
                                                   address: "Home",
                                                   hourlyRate: 20.0)
    transaction.execute()
    let employee = PayrollDatabase.getEmployee(employeeID)
    XCTAssertEqual("Daniel", employee?.name)
    let paymentClassification = employee?.classification
    XCTAssertTrue(paymentClassification is HourlyClassification)
    let hourly = paymentClassification as? HourlyClassification
    XCTAssertEqual(hourly?.hourlyRate(), 20.0)
    XCTAssertTrue(employee?.schedule is WeeklySchedule)
    XCTAssertTrue(employee?.method is HoldMethod)
  }
  
  func testAddCommisionedEmployee() {
    let employeeID = 123
    let transaction = AddCommissionedEmployeeTransaction(employeeID: employeeID,
                                                         name: "Daniel",
                                                         address: "Home",
                                                         salary: 2000.0, commissionRate: 0.1)
    transaction.execute()
    let employee = PayrollDatabase.getEmployee(employeeID)
    XCTAssertEqual("Daniel", employee?.name)
    let paymentClassification = employee?.classification
    XCTAssertTrue(paymentClassification is CommissionedClassification)
    let hourly = paymentClassification as? CommissionedClassification
    XCTAssertEqual(hourly?.salary, 2000.0)
    XCTAssertTrue(employee?.schedule is BiweeklySchedule)
    XCTAssertTrue(employee?.method is HoldMethod)
  }
  

}
