import Foundation
struct HourlyClassification: PaymentClassification, Equatable {
  private let _hourlyRate: Double
  private var timeCards: [TimeCard] = []
  
  public init(hourlyRate: Double) {
    _hourlyRate = hourlyRate
  }
  
  func getTimeCard(_ date: Date) -> TimeCard? {
    return timeCards.first(where: { $0.date() == date })
  }
  
  mutating func addTimeCard(_ timeCard: TimeCard) {
    timeCards.append(timeCard)
  }
  
  public func hourlyRate() -> Double {
    return _hourlyRate
  }
  
  func calculatePay(_ paycheck: Paycheck) -> Double {
    return timeCards.map { (card) -> Double in
      guard DateUtil.isInPayPeriod(date: card.date(), startDate: paycheck.startDate, endDate: paycheck.payDate) else {
        return 0
      }
      return calculatePayForTimeCard(card)
    }.reduce(0, +)
  }
  
  fileprivate func difference(_ date: Date, _ paydate: Date) -> DateComponents {
    return Calendar(identifier: .gregorian).dateComponents([.day],
                                                           from: date,
                                                           to: paydate)
  }
  
  fileprivate func calculatePayForTimeCard(_ card: TimeCard) -> Double {
    return normalHours(card) * _hourlyRate + overtimeHours(card) * overtimeRate()
  }
  
  fileprivate func overtimeHours(_ card: TimeCard) -> Double {
    return max(0, card.hours() - 8)
  }
  
  fileprivate func normalHours(_ card: TimeCard) -> Double {
    return min(card.hours(), 8)
  }
  
  fileprivate func overtimeRate() -> Double {
    return _hourlyRate * 1.5
  }
}
