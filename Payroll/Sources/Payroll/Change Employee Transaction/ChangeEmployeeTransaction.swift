protocol ChangeEmployeeTransaction: Command {
  var employeeID: Int { get }
  func changeEmployee(_ employee: Employee)
}

extension ChangeEmployeeTransaction {
  func execute() {
    if let employee = PayrollDatabase.getEmployee(employeeID) {
      changeEmployee(employee)
    }
  }
}
