import Foundation

struct ServiceCharge: Equatable {
  let date: Date
  let amount: Double
}
