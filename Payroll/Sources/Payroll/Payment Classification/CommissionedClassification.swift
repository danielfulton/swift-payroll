import Foundation
struct CommissionedClassification: PaymentClassification, Equatable {
  let salary: Double
  let commissionRate: Double
  private var receipts: [SalesReceipt] = []
  
  public init(salary: Double, commissionRate: Double) {
    self.salary = salary
    self.commissionRate = commissionRate
  }
  
  func getSalesReceipt(_ date: Date) -> SalesReceipt? {
    return receipts.first(where: { $0.date == date })
  }
  
  mutating func addSalesReceipt(_ receipt: SalesReceipt) {
    receipts.append(receipt)
  }
  
  func calculatePay(_ paycheck: Paycheck) -> Double {
    return salary + receipts(paycheck)
  }
  
  fileprivate func validReceipts(_ paycheck: Paycheck) -> [SalesReceipt] {
    return receipts.filter({ DateUtil.isInPayPeriod(date: $0.date, startDate: paycheck.startDate, endDate: paycheck.payDate) })
  }
  
  fileprivate func receipts(_ paycheck: Paycheck) -> Double {
    return validReceipts(paycheck)
      .map({ $0.amount * commissionRate })
      .reduce(0, +)
  }
  
  fileprivate func difference(_ date: Date, _ paydate: Date) -> DateComponents {
    return Calendar(identifier: .gregorian).dateComponents([.day],
                                                           from: date,
                                                           to: paydate)
  }
}
