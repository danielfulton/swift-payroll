import XCTest
@testable import Payroll

class PaySingleSalariedEmployeeTests: XCTestCase {
  func testPaySingleSalariedEmployee() throws {
    let employeeID = 5678
    let addEmployee = AddSalariedEmployee(employeeID: employeeID, name: "Dan", address: "Home", salary: 1000)
    addEmployee.execute()
    let calendar = Calendar(identifier: .gregorian)
    let timeZone = TimeZone(secondsFromGMT: 0)
    let components: DateComponents = DateComponents(calendar: calendar,
                                                    timeZone: timeZone,
                                                    era: nil,
                                                    year: 2020,
                                                    month: 11,
                                                    day: 30)
    let payDate = try XCTUnwrap(components.date)
    let transaction = PaydayTransaction(payDate: payDate)
    transaction.execute()
    let paycheck = try XCTUnwrap(transaction.getPaycheck(employeeID))
    XCTAssertNotNil(paycheck)
    XCTAssertEqual(payDate, paycheck.payDate)
    XCTAssertEqual(1000, paycheck.grossPay, accuracy: 0.001)
    XCTAssertEqual("Hold", paycheck.getField("Disposition"))
    XCTAssertEqual(0.0, paycheck.deductions, accuracy: 0.001)
    XCTAssertEqual(1000.0, paycheck.netPay, accuracy: 0.001)
  }
  
  func testPaySingleSalariedEmployeeOnWrongDate() throws {
    let employeeID = 123
    let add = AddSalariedEmployee(employeeID: employeeID, name: "Dan", address: "Home", salary: 1000.0)
    add.execute()
    let calendar = Calendar(identifier: .gregorian)
    let timeZone = TimeZone(secondsFromGMT: 0)
    let components: DateComponents = DateComponents(calendar: calendar,
                                                    timeZone: timeZone,
                                                    era: nil,
                                                    year: 2020,
                                                    month: 11,
                                                    day: 29)
    let paydate = try XCTUnwrap(components.date)
    let payday = PaydayTransaction(payDate: paydate)
    payday.execute()
    let paycheck = payday.getPaycheck(employeeID)
    XCTAssertNil(paycheck)
  }
}



