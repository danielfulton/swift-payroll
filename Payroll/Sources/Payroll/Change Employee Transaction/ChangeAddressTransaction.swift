protocol ChangeAddressTransaction: ChangeEmployeeTransaction {
  var newAddress: String { get }
}

struct ChangeAddressTransactionImpl: ChangeAddressTransaction {
  let employeeID: Int
  let newAddress: String
  
  func changeEmployee(_ employee: Employee) {
    var changedEmployee = employee
    changedEmployee.address = newAddress
    PayrollDatabase.deleteEmployee(employeeID)
    PayrollDatabase.addEmployee(changedEmployee)
  }
}
