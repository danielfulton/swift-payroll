import Foundation

struct TimecardTransaction: Command {
  private let date: Date
  private let hours: Double
  private let employeeID: Int
  init(date: Date, hours: Double, employeeID: Int) {
    self.date = date
    self.hours = hours
    self.employeeID = employeeID
  }
  func execute() {
    guard var employee = PayrollDatabase.getEmployee(employeeID) else {
      print("Error: employee not found.")
      return
    }
    guard var hourly = employee.classification as? HourlyClassification else {
      print("Error: Tried to add time card to non-hourly employee.")
      return
    }
    hourly.addTimeCard(TimeCard(date: date, hours: hours))
    employee.classification = hourly
    PayrollDatabase.deleteEmployee(employeeID)
    PayrollDatabase.addEmployee(employee)
  }
}
