@testable import Payroll
import XCTest

class PaySalariedUnionMemberTests: XCTestCase {
  func testUnionMemberDuesPaid() throws {
    let empid = 123
    let add = AddSalariedEmployee(employeeID: empid, name: "Dan", address: "Home", salary: 2000)
    add.execute()
    let memberid = 789
    let change = ChangeMemberTransactionImpl(employeeID: empid, memberID: memberid, dues: 9.42)
    change.execute()
    let paydate = try date(year: 2022, month: 1, day: 31)
    let payday = PaydayTransaction(payDate: paydate)
    payday.execute()
    let paycheck = payday.getPaycheck(empid)
    XCTAssertNotNil(paycheck)
    XCTAssertEqual(paydate, paycheck?.payDate)
    XCTAssertEqual("Hold", paycheck?.getField("Disposition"))
    XCTAssertEqual(37.68, paycheck?.deductions)
    XCTAssertEqual(2000 - 37.68, paycheck?.netPay)
  }
}
