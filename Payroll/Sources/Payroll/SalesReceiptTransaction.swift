import Foundation

struct SalesReceiptTransaction: Command {
  private let date: Date
  private let amount: Double
  private let employeeID: Int
  init(date: Date, amount: Double, employeeID: Int) {
    self.date = date
    self.amount = amount
    self.employeeID = employeeID
  }
  func execute() {
    guard var employee = PayrollDatabase.getEmployee(employeeID) else {
      print("Error: employee not found.")
      return
    }
    guard var commissioned = employee.classification as? CommissionedClassification else {
      print("Error: Tried to add time card to non-hourly employee.")
      return
    }
    let salesReceipt = SalesReceipt(date: date, amount: amount)
    commissioned.addSalesReceipt(salesReceipt)
    employee.classification = commissioned
    PayrollDatabase.deleteEmployee(employeeID)
    PayrollDatabase.addEmployee(employee)
  }
}
