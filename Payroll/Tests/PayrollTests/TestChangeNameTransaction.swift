import XCTest
@testable import Payroll

class TestChangeNameTransaction: XCTestCase {
  func testChangeName() {
    let employeeID = 7
    let addEmployee = AddHourlyEmployeeTransaction(employeeID: employeeID,
                                                   name: "Dave", address: "Home",
                                                   hourlyRate: 15.25)
    addEmployee.execute()
    let changeEmployee: ChangeNameTransaction = ChangeNameTransactionImpl(employeeID: employeeID,
                                                                          newName: "Dan")
    changeEmployee.execute()
    let employee = PayrollDatabase.getEmployee(employeeID)
    XCTAssertNotNil(employee)
    XCTAssertEqual("Dan", employee?.name)
  }
}
