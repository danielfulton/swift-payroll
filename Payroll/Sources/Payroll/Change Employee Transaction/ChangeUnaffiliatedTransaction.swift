protocol ChangeUnaffiliatedTransaction: ChangeAffiliationTransaction {}

extension ChangeUnaffiliatedTransaction {
  func getAffiliation() -> Affiliation {
    return NoAffiliation()
  }
}

struct ChangeUnaffiliatedTransactionImpl: ChangeUnaffiliatedTransaction {
  let employeeID: Int
  let memberID: Int
  
  func recordMembership(employee: Employee) {
    PayrollDatabase.removeUnionMember(memberID)
  }
}
