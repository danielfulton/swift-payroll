import XCTest
@testable import Payroll

class AddServiceChargeTests: XCTestCase {
  func testAddServiceCharge() {
    let id = 7
    let addEmployee = AddHourlyEmployeeTransaction(employeeID: id, name: "Bill", address: "Home", hourlyRate: 43)
    addEmployee.execute()
    guard var employee = PayrollDatabase.getEmployee(id) else {
      XCTFail("Employee not found.")
      return
    }
    let memberID = 189
    let affiliation: Affiliation = UnionAffiliation(memberID: memberID, dues: 1)
    employee.affiliation = affiliation
    PayrollDatabase.addUnionMember(memberID, employee)
    
    let servicChargeDate: Date = Date.distantPast
    let transaction = ServiceChargeTransaction(memberID: memberID, date: servicChargeDate, amount: 12.95)
    transaction.execute()
    
    let serviceCharge = affiliation.getServiceCharge(servicChargeDate)
    XCTAssertNotNil(serviceCharge)
    XCTAssertEqual(serviceCharge?.amount, 12.95)
  }
}


