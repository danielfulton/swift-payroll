protocol ChangeHoldTransaction: ChangeMethodTransaction {}

extension ChangeHoldTransaction {
  func getMethod() -> PaymentMethod {
    return HoldMethod()
  }
}

struct ChangeHoldTransactionImpl: ChangeHoldTransaction {
  var employeeID: Int
}
