protocol ChangeAffiliationTransaction: ChangeEmployeeTransaction {
  func getAffiliation() -> Affiliation
  func recordMembership(employee: Employee)
}

extension ChangeAffiliationTransaction {
  func changeEmployee(_ employee: Employee) {
    let affiliation = getAffiliation()
    var changedEmployee = employee
    changedEmployee.affiliation = affiliation
    recordMembership(employee: changedEmployee)
    PayrollDatabase.deleteEmployee(employeeID)
    PayrollDatabase.addEmployee(changedEmployee)
  }
}
