import XCTest
@testable import Payroll

class ServiceChargesSpanningMultiplePayPeriods: XCTestCase {
  func testServiceChargesSpanningMultiplePayPeriod() throws {
    let empid  = 123
    let memberID = 87687
    let addEmp = AddHourlyEmployeeTransaction(employeeID: empid, name: "Dan", address: "Home", hourlyRate: 12.45)
    addEmp.execute()
    
    let changeMember = ChangeMemberTransactionImpl(employeeID: empid, memberID: memberID, dues: 9.42)
    changeMember.execute()
    
    let payDateFriday = try date(year: 2022, month: 1, day: 14)
    let earlyDate = try date(year: 2022, month: 1, day: 7)
    let lateDate = try date(year: 2022, month: 1, day: 21)
    let serviceChargeTransaction = ServiceChargeTransaction(memberID: memberID, date: payDateFriday, amount: 19.42)
    serviceChargeTransaction.execute()
    let early = ServiceChargeTransaction(memberID: memberID, date: earlyDate, amount: 100)
    early.execute()
    let late = ServiceChargeTransaction(memberID: memberID, date: lateDate, amount: 200)
    late.execute()
    
    let timecard = TimecardTransaction(date: payDateFriday, hours: 8, employeeID: empid)
    timecard.execute()
    
    let payDay = PaydayTransaction(payDate: payDateFriday)
    payDay.execute()
    
    let paycheck = try XCTUnwrap(payDay.getPaycheck(empid))
    XCTAssertEqual(payDateFriday, paycheck.payDate)
    XCTAssertEqual(8 * 12.45, paycheck.grossPay)
    XCTAssertEqual("Hold", paycheck.getField("Disposition"))
    XCTAssertEqual(9.42 + 19.42, paycheck.deductions)
    XCTAssertEqual((8 * 12.45) - (9.42 + 19.42), paycheck.netPay)
  }
}
