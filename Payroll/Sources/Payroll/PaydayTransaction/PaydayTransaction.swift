import Foundation

class PaydayTransaction: Command {
  let payDate: Date
  private var paychecks: [Employee.ID: Paycheck] = [:]
  
  init(payDate: Date) {
    self.payDate = payDate
  }
  
  func execute() {
    let employeeIDs = PayrollDatabase.getAllEmployeeIds()
    for employeeID in employeeIDs {
      if let employee = PayrollDatabase.getEmployee(employeeID) {
        if employee.isPayDate(payDate) {
          let startDate = employee.getPayPeriodStartDate(payDate)
          let paycheck = Paycheck(startDate: startDate, payDate: payDate)
          paychecks[employeeID] = paycheck
          employee.payDay(paycheck)
        }
      }
    }
  }
  func getPaycheck(_ employeeID: Int)-> Paycheck? {
    return paychecks[employeeID]
  }
}
