import Foundation

public struct Employee: Identifiable {
  public let id: Int
  var name: String
  var address: String
  var classification: PaymentClassification
  var affiliation: Affiliation = NoAffiliation()
  var schedule: PaymentSchedule
  var method: PaymentMethod
  
  func isPayDate(_ date: Date) -> Bool {
    return schedule.isPayDate(date)
  }
  
  func getPayPeriodStartDate(_ date: Date) -> Date {
    return schedule.getPayPeriodStartDate(date)
  }
  func payDay( _ paycheck: Paycheck) {
    let grossPay = classification.calculatePay(paycheck)
    let deductions = affiliation.calculateDeductions(paycheck)
    let netPay = grossPay - deductions
    paycheck.grossPay = grossPay
    paycheck.deductions = deductions
    paycheck.netPay = netPay
    method.pay(paycheck)
  }
}
