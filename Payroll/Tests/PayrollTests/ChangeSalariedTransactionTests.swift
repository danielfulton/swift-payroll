import XCTest
@testable import Payroll

class ChangeSalariedTransactionTests: XCTestCase {
  func testChangeSalaried() {
    let addHourly = AddHourlyEmployeeTransaction(employeeID: 5,
                                                 name: "Dan",
                                                 address: "Office",
                                                 hourlyRate: 21.32)
    addHourly.execute()
    let changeSalaried: ChangeSalariedTransaction = ChangeSalariedTransactionImpl(employeeID: 5, salary: 2000)
    changeSalaried.execute()
    let employee = PayrollDatabase.getEmployee(5)
    
    XCTAssertNotNil(employee)
    let pc = employee?.classification
    XCTAssertNotNil(pc)
    XCTAssertTrue(pc is SalariedClassification)
    let salaried = pc as? SalariedClassification
    XCTAssertEqual(salaried?.salary, 2000)
    let schedule = employee?.schedule
    XCTAssertNotNil(schedule)
    XCTAssertTrue(schedule is MonthlySchedule)
  }
}
