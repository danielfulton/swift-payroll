import XCTest
@testable import Payroll

class ChangeDirectTransactionTests: XCTestCase {
  func testChangeMethod() {
    let employeeID = 182736
    let addEmployee = AddHourlyEmployeeTransaction(employeeID: employeeID,
                                                   name: "Dan",
                                                   address: "Home",
                                                   hourlyRate: 12)
    addEmployee.execute()
    let bank = "My Bank"
    let account = "41234234"
    let changeDirect: ChangeMethodTransaction = ChangeDirectTransactionImpl(employeeID: employeeID,
                                                                            bank: bank,
                                                                            account: account)
    changeDirect.execute()
    let employee = PayrollDatabase.getEmployee(employeeID)
    XCTAssertNotNil(employee)
    let paymentMethod = employee?.method
    XCTAssertTrue(paymentMethod is DirectMethod)
    let direct = paymentMethod as? DirectMethod
    XCTAssertEqual(direct?.bank, bank)
    XCTAssertEqual(direct?.account, account)
  }
}
