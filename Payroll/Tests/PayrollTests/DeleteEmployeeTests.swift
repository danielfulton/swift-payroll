import XCTest
@testable import Payroll

class DeleteEmployeeTests: XCTestCase {
  override func setUp() {
    super.setUp()
    PayrollDatabase.reset()
  }
  func testDeleteEmployee() {
    let employeeID = 234
    let addTransaction = AddCommissionedEmployeeTransaction(employeeID: employeeID, name: "Daniel", address: "Home", salary: 3000, commissionRate: 0.1)
    addTransaction.execute()
    var employee = PayrollDatabase.getEmployee(employeeID)
    XCTAssertNotNil(employee)
    let deleteTransaction = DeleteEmployeeTransaction(employeeID: employeeID)
    deleteTransaction.execute()
    employee = PayrollDatabase.getEmployee(employeeID)
    XCTAssertNil(employee)
  }
}
