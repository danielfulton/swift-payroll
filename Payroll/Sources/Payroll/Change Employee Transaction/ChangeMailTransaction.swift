protocol ChangeMailTransaction: ChangeMethodTransaction {
  var address: String { get }
}

struct ChangeMailTransactionImpl: ChangeMailTransaction {
  var employeeID: Int
  var address: String
  
  func getMethod() -> PaymentMethod {
    return MailMethod(address: address)
  }
}
