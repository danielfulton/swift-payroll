struct AddSalariedEmployee: AddEmployeeTransaction {
  let employeeID: Int
  let name: String
  let address: String
  let salary: Double
  func makeClassification() -> PaymentClassification {
    return SalariedClassification(salary: salary)
  }
  func makeSchedule() -> PaymentSchedule {
    return MonthlySchedule()
  }
}
