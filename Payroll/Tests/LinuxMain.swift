import XCTest

import PayrollTests

var tests = [XCTestCaseEntry]()
tests += PayrollTests.allTests()
XCTMain(tests)
