protocol AddEmployeeTransaction: Command {
  var employeeID: Int { get }
  var name: String { get }
  var address: String { get }
  func makeClassification() -> PaymentClassification
  func makeSchedule() -> PaymentSchedule
}

extension AddEmployeeTransaction {
    func execute() {
      let employee = Employee(id: employeeID,
                              name: name,
                              address: address,
                              classification: makeClassification(),
                              schedule: makeSchedule(),
                              method: HoldMethod())
    PayrollDatabase.addEmployee(employee)
  }
}
